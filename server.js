const request = require('request');
const TelegramBot = require('node-telegram-bot-api');
const fs = require('fs');

const token = '';
// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});

// Permet de faire dire ce qu'on veut au bot dans le chan courant
bot.onText(/\/echo (.+)/, (msg, match) => {  
  const chatId = msg.chat.id;
  const resp = match[1]; // ce qui est apres la commande
  bot.sendMessage(chatId, resp);
});

//commande pour set les regles
bot.onText(/\/setRules/, (msg, match) => {
    let authorized = false;
    if (msg.reply_to_message) {
        let configJson = fs.readFileSync('config.json');  
        let config = JSON.parse(configJson);
        for (let i = 0; i < config.su.length; i++) {
            if (msg.from.id == config.su[i]) {
                authorized = true;
            }
        }
        if (authorized) {
            config.rules = msg.reply_to_message.text;
            let dataJson = JSON.stringify(config);  
            fs.writeFileSync('config.json', dataJson);
        }
    }
});

bot.onText(/\/monid/, (msg, match) => {
    bot.sendMessage(msg.chat.id, msg.from.id, {reply_to_message_id:msg.message_id});
});

bot.onText(/\/addsu (.+)/, (msg, match) => {
    if (msg.from.id == "") {
        let configJson = fs.readFileSync('config.json');  
        let config = JSON.parse(configJson);
        config.su.push(match[1]);
        let dataJson = JSON.stringify(config);  
        fs.writeFileSync('config.json', dataJson);
        bot.sendMessage(msg.chat.id, "Ajoute !", {reply_to_message_id:msg.message_id});
    }
});

//commande special nene (oui son cas est pris tres au serieux)
bot.onText(/\/superneneinvincible/, (msg, match) => {
    let neneJson = fs.readFileSync('nene.json');  
    let nene = JSON.parse(neneJson);
    if (nene.unban && nene.count_unban < 3) {
        const chatId = '-';
        var neneId = '';
        bot.unbanChatMember(chatId, neneId);
        bot.sendMessage('','Nene s\'est deban');
        bot.sendMessage('','Go néné, go !');
        nene.unban += 1;
        let dataJson = JSON.stringify(nene);  
        fs.writeFileSync('nene.json', dataJson);
    } else {
        bot.sendMessage('','Non c\'est fini pour aujourd\'hui !');
    }
});

//commande special nene (oui son cas est pris tres au serieux)
bot.onText(/\/donotmutenene/, (msg, match) => {
    let neneJson = fs.readFileSync('nene.json');  
    let nene = JSON.parse(neneJson);
    if (nene.unmute && nene.count_unmute < 3) {
        const chatId = '-';
        var neneId = '';
        bot.restrictChatMember(chatId,neneId,{until_date:Math.round(new Date().getTime()/1000),can_send_messages:true});
        bot.sendMessage('','Nene s\'est demute');
        bot.sendMessage('','Vas y parle !');
        nene.unmute += 1;
        let dataJson = JSON.stringify(nene);  
        fs.writeFileSync('nene.json', dataJson);
    } else {
        bot.sendMessage('','Finito pour today !');
    }
});

//commande special nene (oui son cas est pris tres au serieux)
bot.onText(/\/turnoffnene/, (msg, match) => {
    let neneJson = fs.readFileSync('nene.json');  
    let nene = JSON.parse(neneJson);
    nene.unmute = false;
    nene.unban = false;
    let dataJson = JSON.stringify(nene);  
    fs.writeFileSync('nene.json', dataJson);
});

//commande special nene (oui son cas est pris tres au serieux)
bot.onText(/\/turnonnene/, (msg, match) => {
    let neneJson = fs.readFileSync('nene.json');  
    let nene = JSON.parse(neneJson);
    nene.unmute = true;
    nene.unban = true;
    let dataJson = JSON.stringify(nene);  
    fs.writeFileSync('nene.json', dataJson);
});

//commande special nene (oui son cas est pris tres au serieux)
bot.onText(/\/howmuch/, (msg, match) => {
    let groupsJson = fs.readFileSync('groups.json');  
    let groups = JSON.parse(groupsJson);
    bot.sendMessage(msg.chat.id,"Bot installé dans " + groups.groups.length + " groupes.");
});

//commande pour recuperer les groupes dans lesquels le bot est installe
bot.onText(/\/whereiam/, (msg, match) => {
    let groupsJson = fs.readFileSync('groups.json');  
    let groups = JSON.parse(groupsJson);
    let groupsInstalled = "";
    for (let i = 0; i < groups.groups.length; i++) {
        groupsInstalled += groups.groups[i].id + " : " + groups.groups[i].title + "\n";
    }
    bot.sendMessage(msg.chat.id,groupsInstalled, {reply_to_message_id:msg.message_id});
});

//commande pour recuperer les admins d'un groupe
bot.onText(/\/getadmins (.+)/, (msg, match) => {
    let groupsJson = fs.readFileSync('groups.json');  
    let groups = JSON.parse(groupsJson);
    let adminsList = "";
    for (let i = 0; i < groups.groups.length; i++) {
        if (groups.groups[i].id == match[1]) {
            bot.getChatAdministrators(groups.groups[i].from, {chat_id: groups.groups[i].from}).then(function(admins) {                
                for (let j = 0; j < admins.length; j++) {
                    adminsList += admins[j].user.first_name + "\n";
                }
                bot.sendMessage(msg.chat.id,adminsList, {reply_to_message_id:msg.message_id});
            });
            break;
        }
    }
});

//check IDs des groupes
bot.onText(/\/checkid/, (msg, match) => {
    let groupsJson = fs.readFileSync('groups.json');  
    let groups = JSON.parse(groupsJson);
    for (let i = 0; i < groups.groups.length; i++) {
        if (!groups.groups[i].id || groups.groups[i].id == undefined) {
            groups.groups[i].id = i+1;
        }
        if (!groups.groups[i].title || groups.groups[i].title == undefined) {
            // console.log(groups.groups[i]);
            groups.groups.splice(i,1);
        }
    }
    let dataJson = JSON.stringify(groups);  
    fs.writeFileSync('groups.json', dataJson);
    bot.sendMessage(msg.chat.id,"IDs checked", {reply_to_message_id:msg.message_id});
});

// Permet de diffuser dans tous les groupes une information par certains super utilisateurs
bot.onText(/\/broadcasttoallgj/, (msg, match) => {
    let authorized = false;
    let configJson = fs.readFileSync('config.json');  
    let config = JSON.parse(configJson);
    for (let i = 0; i < config.su.length; i++) {
        if (msg.from.id == config.su[i]) {
            authorized = true;
        }
    }
    if (authorized) {
        let groupsJson = fs.readFileSync('groups.json');  
        let groups = JSON.parse(groupsJson);
        for (let i = 0; i < groups.groups.length; i++) {
            if (msg.reply_to_message.forward_from_chat) {
                // bot.forwardMessage(groups.groups[i].from,msg.reply_to_message.forward_from_chat.id,msg.reply_to_message.forward_from_message_id);
                bot.forwardMessage(groups.groups[i].from,msg.reply_to_message.forward_from_chat.id,msg.reply_to_message.forward_from_message_id);
            }
        }
        bot.sendMessage('','BROADCASTED !!');
    }
});

// Permet de diffuser dans tous les groupes une information par certains super utilisateurs
bot.onText(/\/telltoallgj (.+)/, (msg, match) => {
    const toShare = match[1];
    
    let authorized = false;
    let configJson = fs.readFileSync('config.json');  
    let config = JSON.parse(configJson);
    for (let i = 0; i < config.su.length; i++) {
        if (msg.from.id == config.su[i]) {
            authorized = true;
        }
    }
    if (authorized) {
        let groupsJson = fs.readFileSync('groups.json');  
        let groups = JSON.parse(groupsJson);
        for (let i = 0; i < groups.groups.length; i++) {
            bot.sendMessage(groups.groups[i].from,toShare);
        }
        bot.sendMessage('','BROADCASTED !!');
    }
});


// Ecoute tous les messages et reagit (ou pas) en fonction
bot.on('message', (msg) => {
  const chatId = msg.chat.id;
  let banChatId = '-'

    if (msg.reply_to_message) {
        //Quand nene fait le malin
        if (msg.from.id == '' && (msg.text == 'ban' || msg.text == 'Ban' || msg.text == 'BAN' || msg.text == 'chut' || msg.text == 'Chut' || msg.text == 'supp' || msg.text == 'Supp' || msg.text == 'Mute' || msg.text == 'mute')) {
            let max = 4, min = 0;
            var alea = Math.random() * (max - min) + min;
            alea = Math.round(alea);
            resp = '';
            switch (alea) {
                case 0:
                    resp = 'Nene calme toi tout de suite';
                    break;
                case 1:
                    resp = 'Nene couché !';
                    break;
                case 2:
                    resp = 'Tu peux la fermer 5 minutes ?';
                    break;
                default:
                    resp = 'calmos';
                    break;
            }

            bot.sendMessage(chatId, resp, {reply_to_message_id:msg.message_id});
        }
        let authorized = false;
        //recuperation des admins du chan courant
        bot.getChatAdministrators(chatId, {chat_id: chatId}).then(function(admins) {
            for (let i = 0; i < admins.length; i++) {
                if (msg.from.id == admins[i].user.id) {
                    authorized = true;
                }
            }
            let isSU = false;
            let configJson = fs.readFileSync('config.json');  
            let config = JSON.parse(configJson);
            for (let i = 0; i < config.su.length; i++) {
                // console.log("AUTHORIZED !! => " + config.su[i] + " =? " + msg.from.id);
                if (msg.from.id == config.su[i]) {
                    authorized = true;
                    isSU = true;
                    break;
                }
            }
            if (authorized) {
                if (msg.text == 'regles' || msg.text == 'Regles') {
                    bot.sendMessage(chatId, config.rules, {reply_to_message_id:msg.message_id});
                    reportMsg = msg.from.first_name + ' a rappelé les règles à ' + msg.reply_to_message.from.first_name + ' pour le message suivant : "' + msg.reply_to_message.text + '"';
                    bot.sendMessage(banChatId,reportMsg);
                }
                if (msg.text == 'supp' || msg.text == 'Supp') {
                    bot.deleteMessage(chatId, msg.reply_to_message.message_id);
                    reportMsg = msg.from.first_name + ' a supprimé le message de ' + msg.reply_to_message.from.first_name + ' : "' + msg.reply_to_message.text + '"';
                    bot.sendMessage(banChatId,reportMsg);
                }
                if (msg.text == 'chut' || msg.text == 'Chut') {
                    // bot.deleteMessage(chatId, msg.reply_to_message.message_id);
                    bot.restrictChatMember(chatId, msg.reply_to_message.from.id,{until_date:msg.date+1000});
                    bot.sendMessage(chatId,'Ca fait du bien quand ca s\'arrete :)', {reply_to_message_id:msg.message_id});
                    reportMsg = msg.from.first_name + ' a chuté ' + msg.reply_to_message.from.first_name + ' dans ' + msg.chat.title;
                    bot.sendMessage(banChatId,reportMsg);
                }
                if (msg.text == 'ciao' || msg.text == 'Ciao') {
                    bot.kickChatMember(chatId, msg.reply_to_message.from.id);
                    bot.sendMessage(chatId,'On le reverra pas de si tot', {reply_to_message_id:msg.message_id});
                    reportMsg = msg.from.first_name + ' a banni ' + msg.reply_to_message.from.first_name + ' dans ' + msg.chat.title;
                    bot.sendMessage(banChatId,reportMsg);
                }
                if (msg.text == 'unban' || msg.text == 'Unban') {
                    bot.unbanChatMember(chatId, msg.reply_to_message.from.id);
                }
                if (msg.text == 'promote' || msg.text == 'Promote') {
                    bot.promoteChatMember(chatId, msg.reply_to_message.from.id,{can_edit_messages:true,can_delete_messages:true,can_invite_users:true,can_restrict_members:true});
                }
                if ((msg.text == 'spam' || msg.text == 'Spam') && isSU) {
                    let groupsJson = fs.readFileSync('groups.json');  
                    let groups = JSON.parse(groupsJson);
                    let spammersJson = fs.readFileSync('spammers.json');  
                    let spammers = JSON.parse(spammersJson);
                    spammers.spammers.push({"id":msg.reply_to_message.from.id,"bot":msg.reply_to_message.from.is_bot});
                    for (let i = 0; i < groups.groups.length; i++) {
                        bot.kickChatMember(groups.groups[i].from,msg.reply_to_message.from.id);
                    }
                    bot.sendMessage(chatId,'Et hop, un spammeur en moins !', {reply_to_message_id:msg.message_id});
                    reportMsg = msg.from.first_name + ' a spam ' + msg.reply_to_message.from.first_name + ' dans ' + msg.chat.title;
                    bot.sendMessage(banChatId,reportMsg);
                }
                if (msg.text == 'commandes' || msg.text == 'Commandes') {
                    var resp = "Toutes les commandes fonctionnent lors d'une reponse à un autre message \n\n <b>supp</b> : supprime le message \n\n <b>chut</b> : mute un membre pendant 15 minutes \n\n <b>ciao</b> : ban un membre du groupe \n\n <b>regles</b> : permet d'afficher les regles \n\n <b>promote</b> : permet de passer un membre admin \n\n Seulement pour certains \n\n <b>spam</b> : permet de bannir de tous les groupes un utilisateur \n\n <b>/brodcasttoallgj</b> : diffuse dans tous les groupes ou le bot est present";
                    // send back the matched "whatever" to the chat
                    bot.sendMessage(chatId, resp, {reply_to_message_id:msg.message_id, parse_mode:"HTML"});
                }
            }
        });
    }

    let groupsJson = fs.readFileSync('groups.json');  
    let groups = JSON.parse(groupsJson);
    let exist = false;
    for (let i = 0; i < groups.groups.length; i++) {
        if (groups.groups[i].from == msg.chat.id) {
            exist = true;
            break;
        }
    }
    if (!exist) {
        if (msg.chat.title) {
            groups.groups.push({"from":msg.chat.id, "title":msg.chat.title, "id":groups.groups.length});
            let data = JSON.stringify(groups);
            fs.writeFileSync('groups.json', data);
        }
    }

    if (msg.text == "cafe" || msg.text == "Cafe" || msg.text == "café" || msg.text == "Café") {
        bot.sendMessage(chatId,'HTTP 418 I\'m a teapot', {reply_to_message_id:msg.message_id});
    }
    if (msg.text == "gif" || msg.text == "Gif" || msg.text == "GIF") {
        var gifs = ["14udF3WUwwGMaA","2n8480RCQ2jBe","6fScAIQR0P0xW","xHMIDAy1qkzNS"];
        var alea = Math.random() * eval(gifs.length-1);
        alea = Math.round(alea);
        let gifUrl = "http://i.giphy.com/" + gifs[alea] + ".gif";
        bot.sendVideo(chatId,gifUrl,{reply_to_message_id:msg.message_id});
    }

//   console.log(msg);
  
});